[1,2,3,4].filter(e => e % 2 == 0)
         .map(e => e * 2);


function isPair(e) {
    return e % 2 == 0;
}

function multBy2(e) {
    return e * 2;
}

[1,2,3,4].filter(isPair)
         .map(multBy2);