const fs = require('fs');

// asynchrone non bloquant
fs.readFile('./fs.js', { encoding: 'utf8' }, (err, data) => {
    console.log(data);
});

// fs.readFile('./fs.js', (err, data) => {
//     console.log(data.toString('utf8'));
// });

// API asynchrone stream
fs.createReadStream('./fs.js', { encoding: 'utf8' })
  // .on('data', chunk => console.log(chunk))
  .on('data', console.log)
  .on('end', () => {})
  .on('error', console.log);

  // API asynchrone Promises
fs.promises.readFile('./fs.js', { encoding: 'utf8' })
            .then(console.log)
            .catch(console.log);

// synchrone bloquant
let data = fs.readFileSync('./fs.js', { encoding: 'utf8' });
console.log(data);