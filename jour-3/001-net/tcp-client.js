const net = require('net');

const options = { port: process.env.PORT || 1234 };

const tcpClient = net.createConnection(options, () => {
    console.log(`Connected to server at ${ options.port}`);

    tcpClient.write('hello from the client');
});

let data = "";

tcpClient.on('data', (chunk) => {
    // console.log(chunk instanceof Buffer);
    data += chunk;
    console.log(`received chunk from server ${chunk}`);
});

tcpClient.on('end', () => {
    console.log(`all data received from server ${data}`);
});

tcpClient.on('error', (err) => {
    console.log(err);
});