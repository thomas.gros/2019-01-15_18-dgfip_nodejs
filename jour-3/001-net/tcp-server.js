const net = require('net');

const tcpServer = net.createServer( (socket) => {
    console.log("Some has established a connection");

    socket.on('data', (chunk) => {
        console.log(`received ${chunk}`);
    });

    socket.on('error', (err) => {
        console.log(`Client error: ${err}`);
    });

    socket.write('You are connected to server');
    socket.end('this is the end');

    socket.write('Write after end');

} );

const PORT = process.env.PORT || 1234;

tcpServer.listen(PORT, () => {
    console.log(`server listening on ${ PORT }`);
});

tcpServer.on('error', (err) => {
    console.log(err);
});