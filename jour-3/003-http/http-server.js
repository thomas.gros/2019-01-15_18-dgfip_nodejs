const http = require('http');
const fs = require('fs');

const server = http.createServer( (req, res) => {
    console.log(req.url);
    console.log(req.method);
    console.log(req.headers);

    if(req.url === "/") {
        res.writeHead(200, {
            'Content-Type': 'text/html'
        });
    
        res.end("<h1>ceci est une page web</h1>");
    } else if(req.url === "/code") {
        res.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        // res.statusCode = 200;
        // res.setHeader('Content-Type', 'text/plain');
    
        fs.createReadStream('./http-server.js')
          .pipe(res);
        // res.write('hello')
        // res.end('world');
    } else if (req.url === "/json") {
        res.writeHead(200, {
            'Content-Type': 'application/json'
        });
        const data = { "message": "hello world"};
        res.end(JSON.stringify(data));
    } else {
        res.statusCode = 404;
        res.end("booo");
    }









});

server.on('error', (err) => {
    console.log(err);
});

const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
})