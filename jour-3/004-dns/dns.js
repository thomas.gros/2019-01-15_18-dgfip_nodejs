const dns = require('dns');

dns.lookup('iana.org', (err, address, family) => {
    console.log(`adress: ${address}, family: IPv${family}`);
});

dns.resolve6('iana.org', (err, addresses) => {
    if(err) { console.log(err); return}

    addresses.forEach(a => {
        console.log(a);
        dns.reverse(a, (err, hostnames) => {
            if(err) { console.log(err); return}
            console.log(hostnames);
        })
    })
});

console.log(dns.getServers());