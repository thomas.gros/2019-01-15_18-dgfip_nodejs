const dgram = require('dgram');

const client = dgram.createSocket('udp4');

client.on('error', (err) => {
    console.log(`client error:\n${err.stack}`);
    client.close();
  });

client.on('message', (msg, rinfo) => {
    console.log(`client got: ${msg} from ${rinfo.address}:${rinfo.port}`);
});

client.send(Buffer.from('this is a message from the client'), 
            41234, 
            'localhost', 
            (err) => {
                if(err) {
                   console.log('error sending to server', err);
                }
            });