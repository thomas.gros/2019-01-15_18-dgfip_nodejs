const mongoose = require('mongoose');
mongoose
    .connect('mongodb://mongoadmin:secret@localhost:27017', {
        useNewUrlParser: true,
        dbName: 'mongoose'
    })
    .catch(console.log);

const Cat = mongoose.model('Cat', { name: String });

const kitty = new Cat({ name: 'Zildjian' });
kitty.save()
    .then(() => console.log('meow'))
    .then(() => mongoose.connection.close())
    .catch(console.log);