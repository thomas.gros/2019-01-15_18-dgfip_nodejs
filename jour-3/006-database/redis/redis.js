let redis = require('redis');

/* Values are hard-coded for this example, it's usually best to bring these in via file or environment variable for production */
let client = redis.createClient({
    port: 6379,               // replace with your port
    host: '127.0.0.1',        // replace with your hostanme or IP address
    // password  : 'redis',    // replace with your password

});

client.set('some-key', '42', function (err) {
    if (err) {
        console.log(err);
        throw err; /* in production, handle errors more gracefully */
    } else {
        client.get('some-key', function (err, value) {
            if (err) {
                throw err;
            } else {
                console.log(value);
            }
        });
    }
});