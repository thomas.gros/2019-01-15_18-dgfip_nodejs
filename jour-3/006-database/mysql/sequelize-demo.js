// Attention à ce jour les modules node communautaires
// ne supportent pas le nouveau mécanisme d'authentification node.js
// https://stackoverflow.com/questions/50373427/node-js-cant-authenticate-to-mysql-8-0

const Sequelize = require('sequelize');
const sequelize = new Sequelize('sakila', 'root', 'my-secret-pw', {
  host: 'localhost',
  port: 3306,
  dialect: 'mysql',
  operatorsAliases: false,

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const Film = sequelize.define('film', {
    film_id: {
      type: Sequelize.INTEGER
    },
    title: {
      type: Sequelize.STRING
    }
  });

  Film.findAll().then( console.log ).catch(console.log);
