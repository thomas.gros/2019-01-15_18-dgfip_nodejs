var mysqlx = require('@mysql/xdevapi');
var myTable;
// Connect to server using a connection URL
mysqlx
  .getSession({
    user: 'root',
    password: 'rootroot',
    host: 'localhost',
    port: 33060
  })
  .then(function (session) {
    // Accessing an existing table
    myTable = session.getSchema('sakila').getTable('film');

    // Insert SQL Table data
    return myTable
    //   .insert(['name', 'birthday', 'age'])
    //   .values(['Sakila', '2000-5-27', 16])
    //   .execute()
  })
  .then(function () {
    // Find a row in the SQL Table
    return myTable
        .select(['film_id', 'title'])
        .execute(function (row) {
          console.log(row);
        });
  })
  .then(function (myResult) {
    console.log(myResult);
    myTable.getSession().close();
  }).catch((err) => {
      console.log(err);
  });