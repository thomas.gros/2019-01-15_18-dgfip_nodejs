var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {

  // avant ici on lit de la data dans la bdd;
  let userFromDB = {
    "username": "thomas"
  };

  // res.end('respond with a resource');
  res.render('users/foo', { user: userFromDB });
});

router.post('/', function(req, res, next) {
  res.send('respond with a resource');
});


router.get('/all', function(req, res, next) {
  res.send('all users');
});

router.get('/one', function(req, res, next) {
  res.send('one user');
});

module.exports = router;
