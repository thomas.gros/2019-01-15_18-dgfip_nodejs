var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {

  // avant ici on lit de la data dans la bdd;
  let userFromDB = {
    "username": "thomas"
  };

  res.json(userFromDB);

  // res.json(200, userFromDB);
  //res.setHeader({'Content-Type': 'application/json'});
  //res.end(JSON.stringify(userFromDB));
});

module.exports = router;