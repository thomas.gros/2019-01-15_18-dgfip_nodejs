// console.log(`hello ${process.env.DATA}`);

const http = require('http');

const server = http.createServer( (req, res) => {
    res.end(`hello ${process.env.DATA}`)
})

server.listen(3000);