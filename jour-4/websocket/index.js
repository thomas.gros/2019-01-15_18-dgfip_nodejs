var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

let clients = [];

io.on('connection', (socket) => {
  clients.push(socket);
    console.log('a user connected');
    socket.on('chan-1', (msg) => {
      console.log('message: ' + msg);
      socket.emit('server', 'roger that');
    });

  });


setInterval(() => {
  clients.forEach(c => c.emit('server', 'pop'));
}, 500); 


http.listen(3000, function(){
  console.log('listening on *:3000');
});