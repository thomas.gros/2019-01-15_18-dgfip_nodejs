var assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function() {
    it('should return -1 when the value is not present', function() {
      assert.equal([1,2,3].indexOf(4), -1);
    });

    it('should return index when the value is present', function() {
        let arr = [1,2,3];
        assert.equal(arr.indexOf(2), 1); 
    });
  });
  describe('#push()', function() {
    it('shoud test something');
  });
});

describe('User', function() {
    describe('#save()', function() {
      it('should save without error', function(done) {
        setTimeout( () => {
            assert.ok('test done');
            done(); // <= a appeler pour marquer la fin de test asynchrone
        }, 500);
      });
    });
  });