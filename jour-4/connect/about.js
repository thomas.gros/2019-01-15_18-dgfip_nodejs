module.exports = (req, res, next) => {
    if(req.url === "/about") {
        res.end("page about");
        return;
    }

    next();
}