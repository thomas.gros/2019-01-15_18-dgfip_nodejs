const fs = require('fs');
// async via callback
fs.readFile('./doesnotexist.js', (err, data) => {
    if(err) {
        console.log(err);
        return;
    }

    console.log(data);
});
// async via promises
fsPromise.readFile(...).then().catch();

// async via event-emitters / streams
fs.createReadStream('./doesnotexist.js')
  .on('error', (err) => { console.log(err)});
