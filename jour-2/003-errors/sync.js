function willcrash() {
    throw new Error("boom");
}

try {
    willcrash();
} catch (err) {
    console.log(err);
}

console.log("end");