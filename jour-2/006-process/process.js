process.on('beforeExit', () => {
    console.log('bye bye');
})

console.log(process.cwd());

console.log(process.arch);
console.log(process.platform);
console.log(process.version);
console.log(process.config);

// print process.argv
// voir également https://www.npmjs.com/package/commander
process.argv.forEach((val, index) => {
    console.log(`${index}: ${val}`);
});

// conf=value node process.js
console.log(process.env);



