let x = 42;
// global
global.g = x;
console.log(`dans module-a: ${x}`);

// pas global mais local au module
console.log(__filename);
console.log(__dirname);
// module, exports, require, ne sont pas globaux non plus

// global
console.log(process.env);