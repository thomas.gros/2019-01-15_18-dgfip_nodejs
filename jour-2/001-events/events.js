const EventEmitter = require('events');

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

myEmitter.on('event', () => {
//    process.nextTick(() => console.log('an event occurred!'));
//    setImmediate(() => console.log('an event occurred!'));
    console.log('an event occurred!');
});

myEmitter.on('event', () => {
    console.log('another occurred!');
});

myEmitter.on('error', (err) => {
    console.error('whoops! there was an error');
  });

console.log('avant');
myEmitter.emit('event'); // synchrone
// process.nextTick( () => myEmitter.emit('event')); // async
// setImmediate(() => myEmitter.emit('event')); // async
console.log('après');