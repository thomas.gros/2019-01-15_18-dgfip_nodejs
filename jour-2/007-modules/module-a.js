
// module.exports = 42;
//module.exports = {"hello": "world"};
module.exports.hello = "world";

module.exports.myfunc = function() {
    console.log("hello world");
}

function calc(a, b) {
    return a + b;
}

module.exports.uneAutreFunction = calc;

console.log(module.id)
console.log(module.filename)
