const gzip = require('zlib').createGzip();
const fs = require('fs');

const inp = fs.createReadStream('./readable.js');
const out = fs.createWriteStream('./readable.js.gz');

inp
    .pipe(gzip)
    .pipe(out);