const fs = require('fs');

const fileReadStream = fs.createReadStream('./readable.js', {'highwatermark': 1024*18});

fileReadStream.on('error', (err) => {
    console.log(err);
});

fileReadStream.on('data', (chunk) => {
    console.log(`received: ${ chunk }`);
});

fileReadStream.on('end', () => {
    console.log('end of stream');
});