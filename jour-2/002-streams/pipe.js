const fs = require('fs');

const fileReadStream 
    = fs.createReadStream('./readable.js');
const fsWritableStream 
    = fs.createWriteStream('./copy-readable.js');

fileReadStream
    .pipe(fsWritableStream);