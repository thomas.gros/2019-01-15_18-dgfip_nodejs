function a(f) {
    console.log('avant appel de f');
    f();
    console.log('après appel de f');
}

// callback 1
a(() => {
    console.log('dans le callback');
});

// callback 2
a(function () {
    console.log('dans le callback');
});

// callback 3
function my_func() {
    console.log('dans le callback');
}

a(my_func);

var f = function () {
    console.log('dans le callback');
};

console.log("hello");
[1,2,3,4]
            .filter(e => e % 2 == 0)
            .map(e =>  e * 2)
            .forEach(e => console.log(e))
console.log("world");

         